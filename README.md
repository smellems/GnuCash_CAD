## GnuCash_CAD
Comptes et configuration de taxes pour entreprises au Québec et Canada

## Documentation GnuCash pour entreprise
[Help - Business Features](https://www.gnucash.org/docs/v2.6/C/gnucash-help/chapter_busnss.html)
[Guide - Business Features](https://www.gnucash.org/docs/v2.6/C/gnucash-guide/chapter_bus_features.html)

## Guide pour configuration
### Base
- **Vendor**: Vendeur
- **Customer**: Client
- **Bill**: facture d'un Vendeur payable par l'entreprise
- **Invoice**: facture Client payable à l'entrerpise

### Framagit
`git clone https://framagit.org/smellems/GnuCash_CAD.git`

Démarrer GnuCash et ouvrir le fichier `CAD_Business.gnucash`

### Nouvelle configuration
- File, New File - Suivre les étapes - Remplir informaiton de l'entreprise - Choisir compte entreprise.

- Ajouter comptes pour taxes sous `Liabilities`
 - Taxes/Federal/TPS-GST
 - Taxes/Federal/TVH-HST
 - Taxes/Federal/CTI-ITC
 - Taxes/Provincial/TVQ-QST
 - Taxes/Provincial/RTI-ITR

- Ajouter les taxes - Business, Sales Taxe Table
 - **Bill_QC**: Taxes/Federal/CTI-ITC (5%) et Taxes/Provincial/RTI-ITR (9.975%)
 - **Bill_ON**: Taxes/Federal/CTI-ITC (13%)
 - **Invoice_QC**: Taxes/Federal/TPS-GST (5%) et Taxes/Provincial/TVQ-QST (9.975%)
 
### Exemple d'utilisation
 - Créer un Vendeur de l'Ontario - Table de taxe par défaut: Bill_ON
 - Créer un Vendeur du Québec - Table de taxe par défaut: Bill_QC
 - Créer une facture du vendeur du Qc
  - Regarder `Accounts Payable`
  - Effectuer le payement
 - Créer un Client
 - Créer un facture pour le Client
  - Regarder `Accounts Receivable`
  - Effectuer le payement
